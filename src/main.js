import Vue from 'vue';
import App from './App.vue';
import firebase from 'firebase';
import Vuelidate from 'vuelidate';
import 'babel-polyfill';
import 'bootstrap/dist/css/bootstrap.min.css';

Vue.use(Vuelidate);

firebase.initializeApp({
    apiKey: "AIzaSyArfGSby5EFfRRXoNv-wCYNRcSnVBlDGXM",
    authDomain: "vue-upload-6a36a.firebaseapp.com",
    databaseURL: "https://vue-upload-6a36a.firebaseio.com",
    projectId: "vue-upload-6a36a",
    storageBucket: "vue-upload-6a36a.appspot.com",
    messagingSenderId: "471600995038",
    appId: "1:471600995038:web:b2e293cdce9646f0e9004f",
    measurementId: "G-FD5EXEZZBV"
})

new Vue({
  el: '#app',
  render: h => h(App)
})
